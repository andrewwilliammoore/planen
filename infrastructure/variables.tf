variable "APP_DOMAIN" {}
variable "INSTANCE_PUBLIC_SSH_KEY" {}
variable "TLS_PROXY_PUBLIC_SSH_KEY" {}
variable "ACME_REGISTRATION_EMAIL" {}
variable "EXOSCALE_API_KEY" {}
variable "EXOSCALE_API_SECRET" {}
