module "instance" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/compute-instance-with-docker/0.1.3.zip"

  providers = {
    openstack = openstack
  }

  app_name                = "planen"
  instance_public_ssh_key = var.INSTANCE_PUBLIC_SSH_KEY
}
