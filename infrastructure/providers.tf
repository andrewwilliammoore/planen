terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.40.0"
    }

    exoscale = {
      source  = "exoscale/exoscale"
      version = "0.36.0"
    }

    acme = {
      source  = "vancluever/acme"
      version = "~> 2.8.0"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "3.4.0"
    }
  }
}

provider "openstack" {}

provider "exoscale" {
  # Configuration options
}

provider "acme" {
  #server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

provider "tls" {
  # Configuration options
}
