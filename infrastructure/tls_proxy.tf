module "tls_proxy" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/compute-instance-with-docker/0.1.3.zip"

  providers = {
    openstack = openstack
  }

  app_name                = "planen_tls_proxy"
  instance_public_ssh_key = var.TLS_PROXY_PUBLIC_SSH_KEY
}

resource "local_sensitive_file" "tls_proxy_nginx_conf" {
  # Filename is specific in order to overwrite default server definitions from docker image
  filename = "${path.module}/conf.d/default.conf"

  content = templatefile("${path.module}/default.conf.template", {
    root_domain    = exoscale_domain.root.name
    backend_origin = "http://${module.instance.floating_ip}"
  })

  file_permission = "700"
}

resource "local_sensitive_file" "tls_proxy_tls_cert" {
  filename = "${path.module}/conf.d/cert.crt"
  content  = "${acme_certificate.wildcard.certificate_pem}${acme_certificate.wildcard.issuer_pem}"

  file_permission = "700"
}

resource "local_sensitive_file" "tls_proxy_tls_cert_key" {
  filename = "${path.module}/conf.d/cert.key"
  content  = acme_certificate.wildcard.private_key_pem

  file_permission = "700"
}

resource "null_resource" "tls_proxy_add_ip_to_hosts" {
  provisioner "local-exec" {
    command = "ssh-keyscan ${module.tls_proxy.floating_ip} >> ~/.ssh/known_hosts"
  }
}

# TODO: wait for server to be ready before trying to provision. An error occurs otherwise.
resource "null_resource" "tls_proxy_provisioner" {
  provisioner "local-exec" {
    command = "${path.module}/proxy_deployer.sh"

    environment = {
      DOCKER_HOST     = "ssh://ubuntu@${module.tls_proxy.floating_ip}"
      NGINX_CONF_PATH = "${path.module}/conf.d"
    }
  }

  depends_on = [
    local_sensitive_file.tls_proxy_nginx_conf,
    local_sensitive_file.tls_proxy_tls_cert,
    local_sensitive_file.tls_proxy_tls_cert_key
  ]

  triggers = {
    file_ids = "${local_sensitive_file.tls_proxy_nginx_conf.id}:${local_sensitive_file.tls_proxy_tls_cert.id}:${local_sensitive_file.tls_proxy_tls_cert_key.id}"
  }
}
