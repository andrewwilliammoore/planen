resource "tls_private_key" "wildcard" {
  algorithm = "RSA"
}

resource "acme_registration" "wildcard" {
  account_key_pem = tls_private_key.wildcard.private_key_pem
  email_address   = var.ACME_REGISTRATION_EMAIL
}

resource "acme_certificate" "wildcard" {
  account_key_pem           = acme_registration.wildcard.account_key_pem
  common_name               = "*.${exoscale_domain.root.name}"
  subject_alternative_names = [exoscale_domain.root.name]

  dns_challenge {
    provider = "exoscale"

    config = {
      EXOSCALE_API_KEY    = var.EXOSCALE_API_KEY
      EXOSCALE_API_SECRET = var.EXOSCALE_API_SECRET
    }
  }
}
