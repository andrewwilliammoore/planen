resource "exoscale_domain" "root" {
  name = var.APP_DOMAIN
}

resource "exoscale_domain_record" "www" {
  domain      = exoscale_domain.root.id
  name        = "www"
  record_type = "A"
  content     = module.tls_proxy.floating_ip
}

resource "exoscale_domain_record" "root_domain_asterisk" {
  domain      = exoscale_domain.root.id
  name        = "*"
  record_type = "A"
  content     = module.tls_proxy.floating_ip
}

resource "exoscale_domain_record" "root_domain_at" {
  domain      = exoscale_domain.root.id
  name        = "@"
  record_type = "A"
  content     = module.tls_proxy.floating_ip
}
