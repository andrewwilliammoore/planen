#!/bin/bash

set -eu

. ./bin/.envrc


cd ./app
image_tag=registry.planen.social/planen:$(git rev-parse HEAD)

docker build -t $image_tag . --build-arg NODE_ENV=production
docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD registry.planen.social
docker push $image_tag

cd ../infrastructure
. .envrc
tfenv use
export DOCKER_HOST="ssh://ubuntu@$(terraform output instance_public_ip | tr -d \")"

stop_existing_container () {
  container_name=$1

  if docker container ls -a | grep $container_name; then
    docker stop $container_name
    docker rm $container_name
  fi
}

instance_name=planen_instance
stop_existing_container $instance_name

# TODO: use production command for starting container...
docker run -d \
           --name $instance_name \
           -p 80:3000 \
           -p 443:3000 \
           $image_tag \
           npm start

# TODO: why restart?
# docker restart $instance_name
