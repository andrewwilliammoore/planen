# Planen

## Application

See [README](./app)

## Infrastructure

See [README](./infrastructure)

## Deploy

First time setup. Replace env vars as needed
```bash
cp ./bin/.envrc.example ./bin/.envrc
```

```bash
./bin/deploy.sh
```
