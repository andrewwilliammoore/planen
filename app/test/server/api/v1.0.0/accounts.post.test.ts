import { vi, describe, beforeEach, it, expect } from 'vitest'

import * as apiUtils from 'server/utils/api'
vi.spyOn(apiUtils, 'wrapEventHandler').mockImplementation(fn => fn)

import handler from 'server/api/v1.0.0/accounts.post'

// TODO: add tests to ci
describe('Success', () => {
  beforeEach(() => {
  })

  it('should work', async () => {
    const event = { account: { email: 'someone@test.dev' } }
    const response = await handler(event)

    expect(response).to.equal({})
  })
})
