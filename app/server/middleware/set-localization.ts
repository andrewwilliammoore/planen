import parser from 'accept-language-parser'

import i18n, { defaultLocale } from '../../utils/localizations'

// TODO: determine if everything here necessary
// TODO: redirect root to locale
export default defineEventHandler((event) => {
  console.log('Request:', JSON.stringify(event.req.url))

  const headerLanguages = parser.parse(event.req.headers['accept-language']);
  // Assemble unique array of 2-letter locale codes
  const headerLocales = Object.keys(
    headerLanguages.reduce((all, language) => Object.assign(all, { [language.code]: null }), {})
  )
  const locales = [...headerLocales, defaultLocale]

  // TODO: how to get user locales onto frontend... ?
  event.context.locales = { display: locales[0], content: locales }

  // This setting doesn't take hold on the client side
  //i18n.setLang(event.context.locales.display)

  //console.log('hello set-localizations', event)
})
