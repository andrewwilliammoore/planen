import { defineEventHandler, readBody } from 'h3'

// Wrap event handlers so that functionality can be mocked in tests and also due to the
//   event handling functionality in nuxt being mostly undocumented :(
export const wrapEventHandler = (handler) => {
  return defineEventHandler(async (event) => {
    const body = await readBody(event)

    return handler({ body })
  })
}
