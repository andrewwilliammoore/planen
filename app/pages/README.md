This directory is kept empty because we're using a custom localized routing solution, rather than defining redundant pages for each locale. The `pages` directory is required.
