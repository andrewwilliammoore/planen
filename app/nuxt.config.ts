import { resolve } from 'path'

import { routes, getRoutesForLocale, supportedLocales } from './utils/localizations'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default {
  hooks: {
    'pages:extend' (pages) {
      Object.keys(routes).forEach(name => {
        const routeLocalizations = routes[name]

        Object.keys(routeLocalizations).forEach(locale => {
          pages.push({
            name: `${name}[${locale}]`,
            path: getRoutesForLocale(locale)[name],
            file: resolve(__dirname, `views/${name}.vue`)
          })
        })
      });

      supportedLocales.forEach(locale => {
        // TODO: render root 404 with locale, such as /FAKE to /en/FAKE, etc
        pages.push({
          name: `404[${locale}]`,
          path: '/:pathMatch(.*)*',
          file: resolve(__dirname, 'views/errors/404.vue')
        })
      })
    }
  }
}
