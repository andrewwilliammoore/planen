// TODO: watch this file in dev, currently need to restart app

import i18n from 'i18njs'
import path from 'path'

import de from './de.json'
import en from './en.json'

export const defaultLocale = 'en'
export const supportedLocales = [defaultLocale, 'de']

i18n.add('en', en);
i18n.add('de', de);

export default i18n

export const routes = {
  index: {
    en: '',
    de: ''
  },
  login: {
    en: 'login',
    de: 'einloggen'
  },
  register: {
    en: 'register',
    de: 'registrieren'
  },
}

export const getRoutesForLocale = (locale) => Object.keys(routes).reduce((all, name) => (
  Object.assign(all, { [name]: `/${locale}/${routes[name][locale]}` })
), {})
