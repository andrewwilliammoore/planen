# Planen

## Local Development

`cd` into this directory

In case you want to use node on host machine, package management, etc.
```bash
nvm use
```
```bash
npm ci
```

Run Local Dev Env:

```bash
docker-compose up
```

Locally preview production build:

```bash
docker-compose exec web npm run preview
```
